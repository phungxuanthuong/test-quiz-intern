import React from 'react';
import { Linking } from 'expo';
import { Provider } from 'react-redux';
import store from './redux/store';
import { Dimensions } from 'react-native';
import { BASE_CATEGORY_URL } from './constants/constants';
import Routes from './routes';

const { width, height } = Dimensions.get('window');
const categories = [
  {
    value: 'any',
    text: 'Any Category',
  },
  {
    value: '9',
    text: 'General Knowledge',
  },
];

export default App = () => {
  const prefix = Linking.makeUrl('quiz-app://');
  return (
    <Provider store={store}>
      <Routes uriPrefix={prefix} />
    </Provider>
  );
};

App.navigationOptions = {
  title: 'App',
};
