import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import CountDown from 'react-native-countdown-component';
import { SECOND_PER_QUESTION } from '../constants/constants';

export default class CountTime extends Component {
  render() {
    return (
      <View style={styles.header}>
        <CountDown
          size={30}
          until={SECOND_PER_QUESTION}
          onFinish={() => alert('Finished')}
          style={{
            alignSelf: 'center',
          }}
          digitStyle={{
            backgroundColor: '#FFF',
            borderWidth: 1,
            borderColor: '#1CC4B8',
          }}
          digitTxtStyle={{ color: '#1CC4B8' }}
          timeLabelStyle={{ color: 'red', fontWeight: 'bold' }}
          separatorStyle={{ color: '#1CC4B8' }}
          timeToShow={['M', 'S']}
          timeLabels={{ m: null, s: null }}
          showSeparator
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    marginTop: 20,
    width: '90%',
    alignItems: 'flex-start',
    alignSelf: 'center',
    padding: 10,
  },
});
