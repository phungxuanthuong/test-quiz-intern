import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';

class CategoryQuizScreen extends Component {
  render() {
    return (
      <SafeAreaView style={styles.wrapper}>
        <View style={styles.header}>
          <Text style={styles.textTitle}>Quiz Items</Text>
        </View>
        <View style={styles.content}>
          <TouchableOpacity
            style={styles.itemQuizEnglish}
            onPress={() => this.props.navigation.navigate('Quiz')}
          >
            <Text style={styles.textCategory}>English</Text>
          </TouchableOpacity>
          <View style={styles.viewItemBottom}>
            <TouchableOpacity
              style={styles.itemQuizLogic}
              onPress={() => this.props.navigation.navigate('Quiz')}
            >
              <Text style={styles.textCategory}>Logic</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.itemQuizCoding}
              onPress={() => this.props.navigation.navigate('Quiz')}
            >
              <Text style={styles.textCategory}>Coding</Text>
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity style={styles.viewBottom}>
          <Text style={styles.textStart}>Finish</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

CategoryQuizScreen.navigationOptions = {
  header: null,
};

export default CategoryQuizScreen;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  header: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  content: {
    alignItems: 'center',
    alignSelf: 'center',
    width: '90%',
  },
  itemQuizEnglish: {
    width: 160,
    height: 160,
    backgroundColor: '#1CC4B8',
    marginTop: 20,
    borderRadius: 80,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    position: 'absolute',
  },
  viewItemBottom: {
    flexDirection: 'row',
    width: '90%',
    height: '40%',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'space-between',
    marginTop: 220,
  },
  itemQuizLogic: {
    width: 150,
    height: 150,
    backgroundColor: '#1CC4B8',
    marginTop: -100,
    // marginLeft: 180,
    borderRadius: 75,
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemQuizCoding: {
    width: 150,
    height: 150,
    backgroundColor: '#1CC4B8',
    // marginLeft: 10,
    borderRadius: 75,
    alignItems: 'center',
    justifyContent: 'center',
    // position: 'absolute',
  },
  textCategory: {
    color: '#FFF',
    fontSize: 18,
    fontWeight: 'bold',
  },
  textTitle: {
    color: '#016A9A',
    fontWeight: 'bold',
    fontSize: 30,
    width: '80%',
    textAlign: 'center',
  },
  viewBottom: {
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 0,
    flex: 1,
    backgroundColor: '#016A9A',
  },
  textStart: {
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 30,
  },
});
