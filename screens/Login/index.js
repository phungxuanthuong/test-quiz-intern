import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Picker,
} from 'react-native';

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      language: 'AI',
    };
  }

  handleInputChange = (inputName, inputValue) => {
    this.setState((state) => ({
      ...state,
      [inputName]: inputValue,
    }));
  };

  handleLogin = () => {
    const { email, password, language } = this.state;
    const dataLogin = {
      email,
      password,
      language,
    };
    this.props.navigation.navigate('CategoryQuiz');
  };

  render() {
    return (
      <View style={styles.wrapper}>
        <View style={styles.header}>
          <Image
            source={require('../../assets/logo_osd.jpg')}
            resizeMode={'contain'}
            style={{ width: 300, height: 150 }}
          />
        </View>
        <View style={styles.content}>
          <View style={styles.inputView}>
            <Image
              source={require('../../assets/icon_user.png')}
              resizeMode={'contain'}
              style={{ marginRight: 10 }}
            />
            <TextInput
              style={styles.inputText}
              placeholder='ENTER USERNAME...'
              value={this.state.email}
              onChangeText={(value) => this.handleInputChange('email', value)}
            />
          </View>
          <View style={styles.lineHorizontal} />

          <View style={styles.inputView}>
            <Image
              source={require('../../assets/icon_lock.png')}
              resizeMode={'contain'}
              style={{ marginRight: 10 }}
            />
            <TextInput
              style={styles.inputText}
              placeholder='TYPE PASSWORD...'
              value={this.state.password}
              onChangeText={(value) =>
                this.handleInputChange('password', value)
              }
            />
          </View>
          <View style={styles.lineHorizontal} />

          <View style={styles.selectedView}>
            <Text style={styles.label}>Choose Position</Text>
            <View>
              <Picker
                selectedValue={this.state.language}
                style={{ height: 50, width: 100 }}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({ language: itemValue })
                }
              >
                <Picker.Item label='AI' value='AI' />
                <Picker.Item label='Dev intern' value='Dev intern' />
              </Picker>
            </View>
          </View>
          <TouchableOpacity onPress={this.handleLogin} style={styles.loginBtn}>
            <Text style={styles.loginText}>LOGIN</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

LoginScreen.navigationOptions = {
  header: null,
};

export default LoginScreen;

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
    backgroundColor: '#fff',
  },
  wrapper: {
    alignItems: 'center',
    marginTop: 20,
  },
  content: {
    marginTop: 10,
    width: '90%',
    flexDirection: 'column',
    alignItems: 'center',
  },
  inputText: {
    width: '80%',
  },
  formLogin: {
    alignItems: 'center',
  },
  textInput: {
    flexDirection: 'row',
  },
  inputView: {
    flexDirection: 'row',
    marginTop: 50,
  },
  lineHorizontal: {
    height: 1,
    width: '90%',
    backgroundColor: '#016A9A',
    margin: 10,
  },
  loginBtn: {
    backgroundColor: '#1CC4B8',
    width: 255,
    height: 49,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    margin: 30,
  },
  loginText: {
    color: '#FFFFFF',
    fontWeight: 'bold',
  },
  selectedView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 10,
    width: '90%',
  },
  label: {
    fontSize: 18,
  },
});
