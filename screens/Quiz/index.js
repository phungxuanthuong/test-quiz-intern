import React, { Component } from 'react';
import {
  Text,
  View,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import CountTime from '../../components/CountTime';

class QuizScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isChoose: false,
      progress: 0,
    };
  }

  handleChoose = () => {
    this.setState({
      isChoose: !this.state.isChoose,
    });
  };

  finishExmination = () => {};

  render() {
    const { isChoose } = this.state;

    return (
      <SafeAreaView style={styles.safeAreaView}>
        <View style={styles.header}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('CategoryQuiz')}
          >
            <Image
              source={require('../../assets/Backwardarrow.png')}
              resizeMode={'contain'}
            />
          </TouchableOpacity>
          <CountTime />
          <Text style={{ color: '#006996', fontSize: 16, fontWeight: 'bold' }}>
            10/10
          </Text>
        </View>

        <ScrollView>
          <View style={styles.itemQuiz}>
            <Text style={styles.questionQuiz}>
              <Text style={styles.indexQuestionNCorrect}>1. </Text>The length of
              the bridge, which a train 130 metres long and travelling at 45km/h
              cross in 30 seconds, is:
            </Text>
            <View style={styles.answerQuiz}>
              <TouchableOpacity
                style={[
                  styles.itemAnswer,
                  isChoose ? styles.bgColorChoose : null,
                ]}
                onPress={() => this.handleChoose()}
              >
                <Text style={styles.textAnswer}>
                  <Text style={styles.indexQuestionNCorrect}>A.</Text> 250 m{' '}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.itemAnswer}
                onPress={() => this.handleChoose()}
              >
                <Text style={styles.textAnswer}>
                  <Text style={styles.indexQuestionNCorrect}>B.</Text> 250 m
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.itemAnswer}
                onPress={() => this.handleChoose()}
              >
                <Text style={styles.textAnswer}>
                  <Text style={styles.indexQuestionNCorrect}>C.</Text> 250 m
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.itemAnswer}
                onPress={() => this.handleChoose()}
              >
                <Text>
                  <Text style={styles.indexQuestionNCorrect}>D.</Text> 250 m
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.itemQuiz}>
            <Text style={styles.questionQuiz}>
              <Text style={styles.indexQuestionNCorrect}>2. </Text>The length of
              the bridge, which a train 130 metres long and travelling at 45km/h
              cross in 30 seconds, is:
            </Text>
            <View style={styles.answerQuiz}>
              <TouchableOpacity style={styles.itemAnswer}>
                <Text style={styles.textAnswer}>
                  <Text style={styles.indexQuestionNCorrect}>A.</Text> 250 m
                </Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.itemAnswer}>
                <Text style={styles.textAnswer}>
                  <Text style={styles.indexQuestionNCorrect}>B.</Text> 250 m
                </Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.itemAnswer}>
                <Text style={styles.textAnswer}>
                  <Text style={styles.indexQuestionNCorrect}>C.</Text> 250 m
                </Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.itemAnswer}>
                <Text>
                  <Text style={styles.indexQuestionNCorrect}>D.</Text> 250 m
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

QuizScreen.navigationOptions = {
  header: null,
};
export default QuizScreen;

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },
  viewScroll: {
    marginTop: 10,
  },
  header: {
    marginTop: 10,
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 10,
    flexDirection: 'row',
  },
  itemQuiz: {
    flexDirection: 'column',
  },
  questionQuiz: {
    width: '80%',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 20,
  },
  answerQuiz: {
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: 5,
  },
  itemAnswer: {
    flexDirection: 'row',
    backgroundColor: 'white',
    width: '80%',
    margin: 5,
    padding: 8,
    shadowOpacity: 0.5,
    elevation: 1,
  },
  textAnswer: {
    fontSize: 16,
  },
  indexQuestionNCorrect: {
    color: '#006996',
    fontWeight: 'bold',
    fontSize: 16,
  },
  viewBottom: {
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 0,
    backgroundColor: '#016A9A',
  },
  textStart: {
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 30,
  },
  bgColorChoose: {
    backgroundColor: '#1CC4B8',
  },
  quizInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  time: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  questionRemaining: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#6c63ff',
  },
});
