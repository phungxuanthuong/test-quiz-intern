import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import QuizScreen from './screens/Quiz';
import LoginScreen from './screens/Login';
import CategoryQuizScreen from './screens/CategoryQuiz';
import RoleScreen from './screens/Role';

const QuizStack = createStackNavigator({
  Quiz: QuizScreen,
});

const LoginStack = createStackNavigator({
  Login: LoginScreen,
});

const CategoryQuizStack = createStackNavigator({
  CategoryQuiz: CategoryQuizScreen,
});

// const RoleStack = createStackNavigator({
//   Role: RoleScreen,
// });

// const AppStack = createStackNavigator({
//   Home: HomeScreen,
//   SelectCategory: SelectCategoryScreen,
//   NewPlayer: {
//     screen: NewPlayerScreen,
//     path: 'game/:gameId',
//   },
//   InvitePlayer: InvitePlayerScreen,
//   WaitPlayer: WaitPlayerScreen,
// });

const AppContainer = createAppContainer(
  createSwitchNavigator(
    {
      // Splash: SplashScreen,
      Quiz: QuizStack,
      Login: LoginStack,
      CategoryQuiz: CategoryQuizStack,
      // Role: RoleStack,
      // AppStack,
    },
    {
      initialRouteName: 'Login',
    },
    {
      headerMode: 'none',
    }
  )
);

export default AppContainer;
